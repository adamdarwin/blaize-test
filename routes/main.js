const express = require('express'),
    router = new express.Router();

const Block = require('./../models/block');

let web3 = require('web3');
let inf = new web3(new web3.providers.HttpProvider(process.env.ETH_URL));

/*Get amount of blocks in db*/
router.get('/', async (req, res) => {
    res.send(process.env.blocksAmount || 'Data is not ready');
});

/*
* Get blocks inside interval
* @param    {Number}    start   Interval start
* @param    {Number}    end    Interval end
*
* @return   {Object}    {error, data}
* @param    {String}    error   Query error
* @param    {Array}     data    Array of blocks
* */
router.get('/get_blocks', validate, async (req, res) => {
    let result = await Block.getBlocks(req.query.start, req.query.end);
    return result.error ? res.status(400).send({error: result.error.message}) : res.send(result);
});

function validate (req, res, next) {
    if (!process.env.blocksAmount) return res.status(400).send({error: 'Data is not ready'});
    if (!req.query.start || !req.query.end) return res.status(400).send({error: 'Not enough query params'});
    let start = +req.query.start, end = +req.query.end;
    if (!Number.isInteger(start) || !Number.isInteger(end)) return res.status(400).send({error: 'Params should be number'});
    if (start > end) return res.status(400).send({error: 'Wrong interval'});
    if (end > process.env.blocksAmount) return res.status(400).send({error: `Blocks amount: ${process.env.blocksAmount}`});
    req.query.start = start;
    req.query.end = end;
    return next();
}

module.exports = router;

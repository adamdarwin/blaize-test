const express = require('express'),
    mongoose = require('mongoose');

require('dotenv').config();

mongoose.connect('mongodb://'+process.env.DB_USER+':'+process.env.DB_PASSWORD+'@' + process.env.DB_HOST + ':' + process.env.DB_PORT + '/'+process.env.DB_NAME+'', {useNewUrlParser: true, useUnifiedTopology: true})
    .then(console.log('Mongo connected'))
    .catch(err => {if(err)console.log(err.message)});

let app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use('/', require('./routes/main'));

/*Initialize start batch*/
require('./classes/boot')((data) =>{
    process.env.blocksAmount = data.goodBlocks;
});

app.listen(process.env.PORT || 3000, () => {
    console.log(`Server up on port ${process.env.PORT || 3000}`);
});

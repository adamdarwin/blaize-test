let web3 = require('web3');
let inf = new web3(new web3.providers.HttpProvider(process.env.ETH_URL));

let Block = require('./../models/block');

module.exports = async function(cb){
    try{
        console.log(`Cleaning up mess.`);
        let clean = await Block.cleanUp();
        if (clean.error) throw new Error(clean.error);
        console.log(`Removed ${clean.deletedCount} records from past.`);
        const latest = await inf.eth.getBlockNumber();
        if (!latest) return {error: 'Can\'t get blocks.'};
        let badBlocks = 0, goodBlocks = 0, savingErrors = [];
        console.log(`Batch started`);
        for (let i = 0; i <= (+process.env.BLOCK_RESTRICT || latest); i++) {
            let block = await inf.eth.getBlock(i);
            if (!block){
                badBlocks++;
                continue;
            }
            let saved = await Block.addBlock(block);
            if (saved.error) savingErrors.push({number: i, error: saved.error})
            else goodBlocks++
        }
        console.log(`Blocks received ${goodBlocks}. Bad blocks: ${badBlocks}. Db errors: ${savingErrors.length}`);
        return cb({goodBlocks, badBlocks, savingErrors});
    } catch (error) {
       console.log(`Error while initial batch ${error.message}`);
       return {error};
    }
};

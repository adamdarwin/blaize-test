const mongoose = require('mongoose');

const blockSchema = new mongoose.Schema({
    "difficulty": {
        type: String
    },
    "extraData": {
        type: String
    },
    "gasLimit": {
        type: Number
    },
    "gasUsed": {
        type: Number
    },
    "hash": {
        type: String
    },
    "logsBloom": {
        type: String
    },
    "miner": {
        type: String
    },
    "mixHash": {
        type: String
    },
    "nonce": {
        type: String
    },
    "number": {
        type: Number
    },
    "parentHash": {
        type: String
    },
    "receiptsRoot": {
        type: String
    },
    "sha3Uncles": {
        type: String
    },
    "size": {
        type: Number
    },
    "stateRoot": {
        type: String
    },
    "timestamp": {
        type: Number
    },
    "totalDifficulty": {
        type: String
    },
    "transactions": {
        type: Array
    },
    "transactionsRoot": {
        type: String
    },
    "uncles": {
        type: Array
    }
});

const Block = new mongoose.model('blocks', blockSchema);

/*
* Clean data from last boot
* */
Block.cleanUp = async () => {
    try {
        return await Block.remove({});
    } catch (error) {
        console.log(`Error cleanUp: ${error.message}`);
        return {error};
    }
};

/*
* Add block to collection
* @param    data {Object}    Block object
*
* @returns  {Object}    Saved document or error
* */
Block.addBlock = async (data) => {
    try {
        let block = new Block(data);
        let save =  await block.save();
        return save;
    }catch (error) {
        console.log(`Error while saving block: ${error.message}`);
        return {error};
    }
};

/*
* Return array of blocks in given interval
* @param    {Number}    start   Start of interval
* @param    {Number}    end     End of interval
*
* @return   {Array} Array of documents
* */
Block.getBlocks = async (start, end) => {
    try {
        let data = [];
        let res = await Block.find({}, null, {skip: start}).limit(end - start);
        res.forEach(val => {
            delete val._doc._id;
            delete val._doc.__v;
            data.push(val._doc);
        });
        return {data};
    } catch (error) {
        console.log(`Error while get blocks ${error.message}`);
        return {error};
    }
};

module.exports = Block;